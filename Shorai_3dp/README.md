#Compiling this source in a new Environment#
#==========================================#

It is no secret that migrating C/C++ projects between machine in an Eclipse environment is a daunting tas

There are huge issues if architecture is changed.

Additionally the symbolic links tween projects and real files are often broken and a pain to fix.

The software for STEVAL-3DP001 is no exception, and this, otherwise excellent board does not enjoy the reputation it should.

Compound this to the fact that ST publish greatest support for Atollic and Ac6, both of which are Eclipse based. The supplied project also does not have the .ioc file from cube so a simple regenration of the cube project and device initialisations is impossible. Thank you ST and Bicephale.

MDK Arm and IAR EWARM do not suffer these prolems, but both are expensive, provide poorer IDE environments in code completion, and are limited to ridiculously small footprints in the free versions.

One workaround is to download under a fake user name in an incognito window and use offline.
Another is to get the 1 month evaluation of either MDK or EWARM and get it all working in a moth or two. I can then, in a more leisurely way move the project to makefiles or another IDE. CMake and autoconf could help me here as well. 

However, I am not keen to use dirty tactics. Suffice to see that EWARM compiles the code OK. I should be able to remake the project under Atollic or AC6 and work comfortably.  

In the longer term, it is also probably better to wean off Marlin as this will aways be crippled by the architecture of 40 and more year old device designs.    


## END OF RANT ##

##Things to watch##

Atollic manages to debug quite well 1st time round, therafter, it may dump on stuff it managed previously huh?

###-startup.s### - 
Call to "__libc_init_array" needs to be put back, or maybe we do away with STC_PERIPH_DRIVER as we are using HAL.

###-wifi###
 - end of string NEEDS to be CR, not CR/LF or any other (See ST docs on dynamic pages in WiFi)
 - #define WIFI_DEBUG will get wifi comms on syserr?
 - BSP_WifiParseRxBytes interprets data from wifi
 - @TODO: SD configuration file to show up on the mass storage device for editing
 - A means of updating configuratrion page through http page
 - SSID and WEP_KEY are currently located in main.cpp  Shorai "%UQ>]NA7gd'("  192.168.1.10
   
 
###-stm32f4xx_3dprinter_misc.c###
 - memsets for Extruder fan structs cause fail

### Pin Definitions ###
   Pins are defined in Drivers/BSP/STM32F4xx-3dPrinter/stm32f4xx_3dprinter_misc.h and .c
   Basically the .h file has definiotions for the port and pin per hardware
                 .c file has 2 tables mapping an index to a port and index to pin
   Mapping is done at run time
   A number of pins (e.g. IR I2C etc have been reassigned to Zprobe etc) 
   And quite a few are mapped to -1 in Marlin/pins_STM_#DPRINT.h
   An absolute nightmare
   
   Had to set YMIN_PIN to 7 in pins_ST....h as not using this pin to probe

###SanityCheck.h###
  Needed to change Extruder 2  and 3 (1 and 2) checks as this board does not use enable pins
  
###Temperature.cpp###
Had to change WRITE_softPWM to GPIO_PIN_SET /RESET line +-1442


###Thermistors###
 You need thermistors on all 3 Bed sensors or it won't start. 
      1K resistors read +-137.5 degrees  M105
 You also need on the extruders ;-)

###syserr### 
   is on Debug USB, connect a terminal at 115200  baud, 8 bits no parity  
   Inserted 100ms delay into startups and gcode_M111() in Marlin_main.c
   Otherwise, if USB not txcomplete, we get error 0X1008

### motor config ###
   in Marlin steppers.cpp look for void Stepper::microstep_init()
   I then use the BSP to set overcurrent limits and current 
   I also display on startup
   should provide some M commands to set stepping, power etc
   
### Uppercase ONLY ###
Repetier (and Gcode) assume uppercase in all commands.
I do a character conversion in Libraries/BSP/stm32F4xx-3dPrinter/stm32f4xx_3dPrinter_uart.c

### Serial Port###
  Wifi is a dog, get debug USART working for starters - you're connected via USB anyway
  The code is much too fast for the USB (@115200?) so need some delays
  look in Marlin.h for MYSERIAL macros
  look in MArlinSerial.cpp and .h
  
### USB Timeouts ###
  I had a lot of trouble with Repetier Host and the STeval boarrd. THese all went away when I changed to Octoprint on Linux.
  The resend command in Repetier causes the STeval to reject everything on 'wrong line number' though, in a limited time I could not see the problem either with Repetier or this Marlin. 
  I don't know what the problem is - you need to run in the debugger and pause execution when it happens.  
  Perhaps a newer version of Marlin or HAL will fix it (this is a very old version and needs merge / update from Git).
  Looks like we want to send a \0 and we fail on first byte transmit from L6474_WriteBytes(&spiTxBursts[3][0], &spiRxBursts[3][0]); 
  Looks like manage_inactivity has one too many extruders !!! (4???? should be 3!!!!)
  also lets set systick priority explicitly high (Stack Exchange) NVIC_SetPriority(SysTick_IRQn, 0); in HAL_msp.c
               this is done in the SYstickInit by main.cpp and was Ok
       and lets set SPI there as well 
  The original uses PRiority grouping 4 (16 levels, 0 sublevels) so we can play number 0..15 to get everything OK
  Motror control_misc.c setup seems to override / conflict BSP_MiscOverallInit priority grouping is 3 !!!!!! (8 levels of two)
  in mootorControl.h SPIx is SPI1  ...... need to setup priority
  
  May be SPI is at higher priority than systick ?????
  - SPI_WaitOnFlagUntilTimeout() :2 219
  - HAL_SPI_TransmitReceive() :808
  - BSP_MotorControlBoard_SPI_writeBytes() :580
  - L6474_WriteBytes : 1 260
  - L6474_SendCommand() :1 260
  - L6474_CmdDisable() :728
  - BSP_MOTORControl_CmdDisable() motorControl.c:797
  - manage_inactivity() MArlinMAin:8 604
  - idle()
  - loop()  
  
  Fixing the above has made USB
   MUCH more stable (Setting priority, removing Extruder_3 from Marlin. Extruders are E0, E1, E2
   
### Stepper::StepperHAndler###
This si supposed to be an Isr ????
Really??? 
The author obviously does not know what they are doing, much too long
TL;DR -- Didn't extecute Execute the programmer
Probably rewrite to 3 pairs per motor (accelerating, count;  plateau; count; decelleration, count) or similar

  
## HAL Interrupt setup ##
The basic board HAL_SetPriority_Grouping i s _3 (16 levels, no sublevels) 
The IRQs I can find are

Libraries/Drivers/BSP/STM32F4xx-3dPrinter/stm32f4xx_3dprinter_uart.c:  HAL_NVIC_SetPriority(BSP_UART_DEBUG_IRQn, 1, 0);\

Libraries/Drivers/BSP/STM32F4xx-3dPrinter/stm32f4xx_3dprinter_motor.c:  HAL_NVIC_SetPriority(BSP_MOTOR_CONTROL_BOARD_FLAG_IRQn, 7, 0);
Libraries/Drivers/BSP/STM32F4xx-3dPrinter/stm32f4xx_3dprinter_sd.c:  HAL_NVIC_SetPriority(BSP_SD_DETECT_IRQn, BSP_SD_DETECT_PRIORITY, 0);
Libraries/Drivers/BSP/STM32F4xx-3dPrinter/stm32f4xx_3dprinter_sd.c:  HAL_NVIC_SetPriority(SDIO_IRQn, 1, 0);
Libraries/Drivers/BSP/STM32F4xx-3dPrinter/stm32f4xx_3dprinter_sd.c:  HAL_NVIC_SetPriority(BSP_SD_DMAx_Rx_IRQn, 2, 0);
Libraries/Drivers/BSP/STM32F4xx-3dPrinter/stm32f4xx_3dprinter_sd.c:  HAL_NVIC_SetPriority(BSP_SD_DMAx_Tx_IRQn, 2, 0);

then in src/stm32f4xx_hal_msp.c:

UART_DEBUG (3,0) 
WIFI  Tx (4,1)  ?? Really!!?? 
WIFI RX (4,0) 
MCB_PWMX (6,0)
MCB_WM_Y (6,0)
MCB_PWM Z, E0, E1, E2 (5,0)
TICK   ( 0,0) 
TICK2  (5,0) 
SERVO   1,0 
DMA   (7,1) 
ADC   (7,0) 

I set SPI to (3, 0)  

  

##Startup tests ##
Try the following commands

M115 - Get firmware version
M105 - reports temperatures
M114 - Reports XYZ 
M119 - REports Endstops
G28  - home all
G92  - set XYZE to position
M104 - set extruder temperature
M106 - set fan speed S0..255
M107 - turn fan off
M245/6  - start/stop cooler fan


##Moving to your Environment##
I have found the following appears to work for what I have set up under 'First STab' below.

I chose Atollic as I think it is one of the better portable IDEs around, thoug I don't like Eclipse.
It has no code size limits and has a good debugger. There are free and paid versions so it is good if my product taks off.


1) Create a Directory to be used as workspace  
    md steval_3dp001. This can be anywhere, and maybe you can call it anything (beware of total Eclipse)
    cd steval_3dp001

2) Clone my repository
    git clone https://crowse@bitbucket.org/crowse/steval_3dp001.git

3) Start Atollic 
    Point your workspace to steval_3dp001 created above
    Open -> project from files -> point to SHorai_3dp

4) go to properties and add the following 
    C/C++ Build -> C compiler -> Symbols
        MARLIN
        STEVAL_3DP001
        STM32F4xx
        STM32F401xE
        STM32F40xx
The last  should be unnecessary
        
   #define BOARDNAME STM "EVALR3DPRINT"  in pins_STM_3DPRINT.h
   #elif MB(STM_3DPRINT)   in pins.h (huh?)
   

VECT_TAB_SRAM  causes initialisation problems and immediate loss of return stack in ssytem_stm32f4xx.c line 250
      where the vestor table is relocated. Maybe we need to initialize ram vector table first?
        

5) Go to build->Target and select MCU-STM32F401xE

6) Go to Properties -> C/C++ Build -> Settings-> Tool Settings->C++Compiler->General
      Select c++11 from the drop down 
      I am no tsure if this is correct we may want gnu++11 or gnu++14 
      This affects the startup code in startup.s and system_stm32f4xx.c
        
   Go to Properties -> C/C++ build -> Settings-> Tool Settings ->C++Linker->General ->Linker Script
       You should be able to leave this blank. If it cant find the scripts then
      Check the Linker script points to the stm32f4_flash.ld file in the project root directory      

7) You should now get a clean compile and be able to flash
      There are a few FIXMEs, TODOs
      I am replacing the entire set of Serial port macros and classes as the Marlin ones are somewhat tatty. I may regret this as the WiFi may break :-(
      I want to have at least the following

   a) Virtual com port on Debug connector
   b) Virtual com port on OTG connector
   c) Http access via the Wifi
   d) Telnet/sockets access via WiFi (port 22) 
   
   Of course, at this stage even one of the above would be SUPERB. But then this is ST isn't it?
   
  
   
##Status##

####2019-02-28####
1. XYZ axes respond to G00 commands, Extruders probably need thermistors at temp
2. Compiled in config active
3. M151 active
4. M105 seems Ok,gets bed temp, extruder thermistors not present
5. M119 QUery endstops seems Ok
6. COMMANDS ARE ALL IN CAPS

##DOING##


##TODO##

. Get everything working and tested to Marlin 1.1.0-RC7, convert to latest
. Get Serial port access to the drivers as above
. Add Http protocol to enable Klipper to be used to drive 
. Move Libraries code to a libraries project in the workspace
. Reorganize the AVR junk out of the libraries
. Refactor marlin libraries serial port usage to stdin and stderr via newlib. Makes it more portable as well known. 
. Build a FORTH based protocol http://<ip>/forth   
. Do we want/need vector table in ram?
. complete definition of Cube MX model .ioc file so we can use cube to generate startup code and symbols in the future
. Move to latest version of Marlin
. Move to latest version of ST_HAL libs and startup 
. get all IO through newlib. Then provide file handles which are swappable
. stdin/stdout default for GCode and responses, stderr for system messages. Possibly move to USB port.
. Wifi connectivity
. stm3dp..../usart.c has some glitches
 	// @TODO - expanding \n to \n\r this is unsafe and probably not required, buffer overflow!!!!!
   	// @TODO - the code only sends on a \n, unterminated strings May cause problems of not sending if last character is not a \n, we NEVER send, and timeout
   	Looks like the NVIC was not set for BSP_UART_DEBUG_IRQn === very low priority and voerrun errors
   	Check and clear ORE bit if RXNE shows empty!!!!! (Known bug?)
   
## Done##
2019-02-28 Removed some unused defines
2019-02-28 Get rid of undesired defines - USE_STD_PERIPHDRIVER, STM32F4XX,  ARM_MATH_CM4
	get rid of stm32f4xx_conf.h - hangover from std_periph_libs
       VECT_TABLE_SRAM was taken out (can probably be put back) 
      Now have a problem in system_stm32f4xx.systeminit() where VEctor table is relocated. 
      At this point, the return stack goes wonky and the reference to Reset_handler line 104 bl System_Init is lost.
      We need to be more careful in this? 
      Looks like we have a broken startup code, 
      Startup code is specific to the compiler (arm, gcc, iar)  Attollic uses arm-attollic-eabi-*
      Latest version of startup.c does not have a setSystemClocks() routine at all 
      Removing SetSystemClocks seems to have cleared everything. The startup is good and I am getting messages on /dev/ttyACM0 at 115200 baud. YAY!!! :-)
      Added a Delay in MarlinStart as processes beat Serial's ability to send 
      Process dies on lack of Bed thermistor (Need to fit these).
      AFter fitting three thermistors to the bed, the machine starts and can do at least some G and M commands
      As very little was changed in the Marlin and ST portions, it is probable that everything works +- to the spec of March 2017 when ST last checked the code in. 
      SD card is not providing a directory listing
      WiFi is not a priority at present. Need to check all other components and parts.
     

#1) First stab Conversion to Atollic#
#-----------------------------------#

THis section is completed, but may help someone porting this or another piece of code elsewhere

I am looking for a quick fix to the project files as I know that the source compiles OK under EWARM. It won't link as it is over 32Kb.

1.1) Open Atollic and generate a new C/C++ project. Check that it compiles.

1.2) Copy and paste 
    1.2a) Copy Device, Drivers FatFs and Marlin subdirectories into Libraries folder.
		These should eventually be in a Library project
1.3) copy stm32f4xx_hal_msp.c into src/ folder 
     copy Inc/  from projects/..../Inc to src folder
     copy stm32f4xx_it.c over the file in src/stm32f4xx_it.c

1.4) Right click the project, ->properties->C/C++General->Paths and Symbols
    1.4a) Get 3DP project defines working
           #Symbols tab
           Click on GNU C
           add ARM_MATH_CM4
           add STM32F401xE
           add MARLIN
               check add to all configurations and languages
            click Apply then Ok
           in C++ define STD c++11, apply Ok
    1.4b) Add reference to include files
         Includes tab
         Under GNU C add paths to the following. In each case add to all environments and all languages
         In each case make it a workspace reference
         (copy paste from this list)
         
         Libraries/Drivers/BSP/Components/Common
         Libraries/Drivers/BSP/Components/l6474
         Libraries/Drivers/BSP/MotorControl
         Libraries/Drivers/STM32F4xx_HAL_Driver/Inc
         Libraries/FatFs/src
         Libraries/Marlin
         Inc
         Libraries/Drivers/BSP/STM32F4xx-3dPrinter
These entries should all be in the list in BOLD.
Unfortunately Eclipse does not let you click and modify them in the project explorer. You have to go to  project properties 
         click <Apply> <Ok>

1.4c) Exclude from the build
          Libraries/FatFs/src/option
          Libraries/FatFs/doc
          Libraries/CMSIS/DSP
        
1.4d) Set C++11  
          Project->C/C++ BUild/Settings/Tools/GEneral
              choose C++11

1.5) Define your WIFI connection
        Marlin/Configuration.h
            WIFI_SSID   DO_F941
            WIFI_WEP_KEY LMIHFSXX
            BAUDRATE 115200


1.6) Code fixups
        1.6a) - Includes missing in stmf4xx_it.c  for ADC, WIFI, USART headers
                This resus in some structs being undeclared and assumed ints, compiler message misleading

1.6b) - MarlinSerial is a mess largely due to non existence of decent HAL in C++ either Arduino or ST
                Eventually we will take all of these out and replace with HAL calls to DMA  :-)     



## Reimplementation in FORTH ##
see implementation.txt for more details



