#ifndef __CONSOLE__
#define __CONSOLE__
/**
 * This class is a header only class and defines the Serial handlers necessary to
 *  commuicate between our host and 3D printer.
 *
 *  Comms are dependent on the CPU and libraries being used, and we can thus encapsulate
 *     whatever differences and local conditions into this file
 *
 *  In the case of Atollic, there is already defined printf etthat access the virtual comms port
 *     on the debug connector
 *
 *  There is also wifi etc
 *
 *
 */

#include <stdio.h>
#include "stm32f4xx_3dprinter_uart.h"


#define DEC 10
#define HEX 16
#define OCT 8
#define BIN 2
#define BYT 0

class Console {

public:
	  // begin should allow us to supply or list a parameter that gives WiFi, VCP, serial or any other Stream device
	  void begin(long baudrate) {

	  }
	  int available(void) {return (
			  BSP_UartGetNbRxAvalaibleBytes());
	  }

	  void checkRx(void) {}
	  void write(uint8_t c) { BSP_UartIfQueueTxData(&c, 1); }

	  void flush() {BSP_UartIfSendQueuedData();}


	  int read(void) {return BSP_UartGetNextRxBytes();}
	  void write(const char* str) { while (*str) write(*str++); }
	  void write(const uint8_t* buffer, size_t size) { while (size--) write(*buffer++); printf("%s",buffer); }
	  void print(const char* str) { write(str); printf("%s",str); }
	  void printn(uint8_t *str, uint8_t nbData) { BSP_UartIfQueueTxData(str, nbData); }

	/*void write(const char *str) { puts(str); }
	void write(char ch) {
		 char str[2];
		 str[0] = ch;
		 str[1] = 0;
		 write(str); }

    void print(char, int = BYT);
    void print(unsigned char, int = BYT);

    void print(int i, int base= DEC) {
    	char str[10];
    	sprintf(str,"%d",i);
    	write(str);
    }
*/
    void print(unsigned int n, int base = DEC) {
    	char str[10];
    	sprintf(str,"%d",n);
    	write(str);
    }
/*
    void print(long n, int base= DEC) {
    	char str[10];
    	sprintf(str,"%ld",n);
    	write(str);
    }

    void print(unsigned long n, int base = DEC){
    	char str[10];
    	sprintf(str,"%ul",n);
    	write(str);
    }
    void print(double, int = 2){}
*/
    void nl() {
    	puts("\r\n");
    }

void println(char *x) { print(x); nl(); }

//#define println(X,Y) print(X,Y); nl();
/*
    void println(const char[]);
    void println(char, int = BYT);
    void println(unsigned char, int = BYT);
    void println(int, int = DEC);
    void println(unsigned int, int = DEC);
    void println(long, int = DEC);
    void println(unsigned long, int = DEC);
    void println(double, int = 2);
    void println(void);
    */

private:


};



#endif
