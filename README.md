#Compiling this source in a new Environment#
#==========================================#

It is no secret that migrating C/C++ projects between machine in an Eclipse environment is a daunting tas

There are huge issues if architecture is changed.

Additionally the symbolic links tween projects and real files are often broken and a pain to fix.

The software for STEVAL-3DP001 is no exception, and this, otherwise excellent board does not enjoy the reputation it should.

Compound this to the fact that ST publish greatest support for Atollic and Ac6, both of which are Eclipse based. The supplied project also does not have the .ioc file from cube so a simple regenration of the cube project and device initialisations is impossible. Thank you ST and Bicephale.

MDK Arm and IAR EWARM do not suffer these prolems, but both are expensive, provide poorer IDE environments in code completion, and are limited to ridiculously small footprints in the free versions.

One workaround is to download under a fake user name in an incognito window and use offline.
Another is to get the 1 month evaluation of either MDK or EWARM and get it all working in a moth or two. I can then, in a more leisurely way move the project to makefiles or another IDE. CMake and autoconf could help me here as well. 

However, I am not keen to use dirty tactics. Suffice to see that EWARM compiles the code OK. I should be able to remake the project under Atollic or AC6 and work comfortably.  

In the longer term, it is also probably better to wean off Marlin as this will aways be crippled by the architecture of 40 and more year old device designs.    


### END OF RANT ###

## quick instructions ##
 Clone this repository
 Open Atollic True Studio
 Set the workbench to wherever these files land (...../steval_3dp001")
 Open the project TestProject
 Compile
 Flash
 Debug and extend
 Push to the rest of us


##1) First stab Atollic##
---------------------

I am looking for a quick fix to the project files as I know that the source compiles OK under EWARM. It won't link as it is over 32Kb.

Open Atollic anf generate a new C/C++ project. Check that it compiles.

1. Copy and paste 
    Copy Device, Drivers FatFs and Marlin subdirectories into Libraries folder.

2. Copy stm32f4xx_hal_msp.c into src/ folder 
     copy Inc/  from projects/..../Inc to src folder
     copy stm32f4xx_it.c over the file in src/stm32f4xx_it.c

3. Right click the project, ->properties->C/C++General->Paths and Symbols
    3.1 Get 3DP project defines working
           Symbols tab
           Click on GNU C
           add ARM_MATH_CM4
           add STM32F401xE
           add MARLIN
               check add to all configurations and languages
            click Apply then Ok
           in C++ define STD c++11, apply Ok

    3.2 Add reference to include files
         Includes tab
         Under GNU C add paths to the following. In each case add to all environments and all languages
         In each case make it a workspace reference

         (copy paste from this list)
         Libraries/Drivers/BSP/Components/Common
         Libraries/Drivers/BSP/Components/l6474
         Libraries/Drivers/BSP/MotorControl
         Libraries/Drivers/STM32F4xx_HAL_Driver/Inc
         Libraries/FatFs/src
         Libraries/Marlin
         Inc
         Libraries/Drivers/BSP/STM32F4xx-3dPrinter

         These entries should all be in the list in BOLD. 
         click <Apply> <Ok>

    3.3 Exclude from the build
          Libraries/FatFs/src/option
          Libraries/FatFs/doc
          Libraries/CMSIS/DSP
        
    3.4 Set C++11  
          Project->C/C++ BUild/Settings/Tools/GEneral
              choose C++11

4. Define your WIFI connection
        src/Main.cpp
            WIFI_SSID   DO_F941
            WIFI_WEP_KEY LMIHFSXX
            BAUDRATE 115200


5. Code fixups
        - Includes missing in stmf4xx_it.c  for ADC, WIFI, USART headers
                This resus in some structs being undeclared and assumed ints, compiler message misleading

        - MarlinSerial is a mess largely due to non existence of decent HAL in C++ either Arduino or ST
                Eventually we will take all of these out and replace with HAL calls to DMA  :-)     




## TODO ##

###The program now compiles Ok but will not run in the debugger###
This may be due to the BOOTP switch being incorrect 

###Fixup the Console Class###
   I have defined a Console class, there should also be an ERRORLOG class
   This needs to be checked as ALL IO IS MAPPED THROUGH IT (as far as I can see)
   The class probably needs to be subclassed or interfaced from the Aduini Serial class
   I need to add a means of switching between VCP on debug to WiFi

###Wifi Passwords ### 
   These are hard coded into main.cpp they should come from environment variables at compile time


###Fixup the code above and check all is there ###
    You can see most of what I have done code wise in the GIT history, especially if you use a git explorer

### Move code to Netbeans, Geany, EmBitz or Anjuta ###
   This is mandatory as Eclipse is very broken for /C++ project portability
   Netbeans is well supported and has done C/C++ for many years
   Also move to something like CMake or autoconf 

 # ======================================================================================================== #


**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
