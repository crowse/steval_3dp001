Compiuing this source in a new Environment
==========================================

It is no secret that migrating C/C++ projects between machine in an Eclipse environment is a daunting tas

There are huge issues if architecture is changed.

Additionally the symbolic links tween projects and real files are often broken and a pain to fix.

The software for STEVAL-3DP001 is no exception, and this, otherwise excellent board does not enjoy the reputation it should.

Compound this to the fact that ST publish greatest support for Atollic and Ac6, both of which are Eclipse based. The supplied project also does not have the .ioc file from cube so a simple regenration of the cube project and device initialisations is impossible. Thank you ST and Bicephale.

MDK Arm and IAR EWARM do not suffer these prolems, but both are expensive, provide poorer IDE environments in code completion, and are limited to ridiculously small footprints in the free versions.

One workaround is to download under a fake user name in an incognito window and use offline.
Another is to get the 1 month evaluation of either MDK or EWARM and get it all working in a moth or two. I can then, in a more leisurely way move the project to makefiles or another IDE. CMake and autoconf could help me here as well. 

However, I am not keen to use dirty tactics. Suffice to see that EWARM compiles the code OK. I should be able to remake the project under Atollic or AC6 and work comfortably.  

In the longer term, it is also probably better to wean off Marlin as this will aways be crippled by the architecture of 40 and more year old device designs.    


## END OF RANT ##

1) First stab Atollic
---------------------

I am looking for a quick fix to the project files as I know that the source compiles OK under EWARM. It won't link as it is over 32Kb.

1.1) Open Atollic anf generate a new C/C++ project. Check that it compiles.

1.2) Copy and paste 
    1.2a) Copy Device, Drivers FatFs and Marlin subdirectories into Libraries folder.

    1.2d) 


1.3) copy stm32f4xx_hal_msp.c into src/ folder 
     copy Inc/  from projects/..../Inc to src folder
     copy stm32f4xx_it.c over the file in src/stm32f4xx_it.c

1.4) Right click the project, ->properties->C/C++General->Paths and Symbols
    1.4a) Get 3DP project defines working
           #Symbols tab
           Click on GNU C
           add ARM_MATH_CM4
           add STM32F401xE
           add MARLIN
               check add to all configurations and languages
            click Apply then Ok
           in C++ define STD c++11, apply Ok

    1.4b) Add reference to include files
         Includes tab
         Under GNU C add paths to the following. In each case add to all environments and all languages
         In each case make it a workspace reference

         (copy paste from this list)
         Libraries/Drivers/BSP/Components/Common
         Libraries/Drivers/BSP/Components/l6474
         Libraries/Drivers/BSP/MotorControl
         Libraries/Drivers/STM32F4xx_HAL_Driver/Inc
         Libraries/FatFs/src
         Libraries/Marlin
         Inc
         Libraries/Drivers/BSP/STM32F4xx-3dPrinter

         These entries should all be in the list in BOLD. 
         click <Apply> <Ok>

    1.4c) Exclude from the build
          Libraries/FatFs/src/option
          Libraries/FatFs/doc
          Libraries/CMSIS/DSP
        
    1.4d) Set C++11  
          Project->C/C++ BUild/Settings/Tools/GEneral
              choose C++11

1.5) Define your WIFI connection
        Marlin/Configuration.h
            WIFI_SSID   DO_F941
            WIFI_WEP_KEY LMIHFSXX
            BAUDRATE 115200


1.6) Code fixups
        1.6a) - Includes missing in stmf4xx_it.c  for ADC, WIFI, USART headers
                This resus in some structs being undeclared and assumed ints, compiler message misleading

        1.6b) - MarlinSerial is a mess largely due to non existence of decent HAL in C++ either Arduino or ST
                Eventually we will take all of these out and replace with HAL calls to DMA  :-)     






